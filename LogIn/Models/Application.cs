﻿using System.Collections.Generic;

namespace LogIn.Models
{
    public class Application
    {
        public long Id { get; set; }
        public string Code { get; set; }
        public string Description { get; set; }
        public string URL { get; set; }
        public ICollection<User> Users { get; set; }
    }
}