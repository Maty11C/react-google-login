﻿using System.Collections.Generic;
using System.Linq;

namespace LogIn.Models
{
    public class User
    {
        public long Id { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string Email { get; set; }
        public string Password { get; set; }
        public ICollection<Application> Applications { get; set; }

        public bool AppAllowed(string applicationCode) => Applications.Any(x => x.Code == applicationCode);

        public string FullName() => $"{FirstName} {LastName}";
    }
}
