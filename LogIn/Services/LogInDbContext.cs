﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using LogIn.Models;
using Microsoft.EntityFrameworkCore;

namespace LogIn.Services
{
    public class LogInDbContext : DbContext
    {
        public LogInDbContext(DbContextOptions options) : base(options) {}
        public DbSet<User> Users { get; set; }
        public DbSet<Application> Applications { get; set; }
    }
}
