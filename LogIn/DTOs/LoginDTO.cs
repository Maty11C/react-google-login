﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace LogIn.DTOs
{
    public class LoginDTO
    {
        public string Email { get; set; }
        public string ApplicationCode { get; set; }
    }
}
