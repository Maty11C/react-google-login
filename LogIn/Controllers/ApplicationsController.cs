﻿using System.Linq;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using LogIn.Models;
using LogIn.Services;

namespace LogIn.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class ApplicationsController : ControllerBase
    {
        private readonly LogInDbContext _context;

        public ApplicationsController(LogInDbContext context)
        {
            _context = context;
        }

        // GET: api/Applications
        [HttpGet]
        public IActionResult GetApplications()
        {
            var applications = _context.Applications.Include(x => x.Users).ToList();

            return Ok(applications);
        }

        // GET: api/Applications/5
        [HttpGet("{id}")]
        public IActionResult GetApplication(long id)
        {
            var application = _context.Applications.Include(x => x.Users).FirstOrDefault(x => x.Id == id);

            if (application == null)
                return NotFound();

            return Ok(application);
        }

        // PUT: api/Applications/5
        [HttpPut("{id}")]
        public IActionResult PutApplication(long id, Application application)
        {
            if (id != application.Id)
                return BadRequest();

            var newApplication = _context.Applications.FirstOrDefault(x => x.Id == id);

            if (newApplication == null)
                return NotFound();

            newApplication.Code = application.Code;
            newApplication.Description = application.Description;
            newApplication.URL = application.URL;
            newApplication.Users = application.Users;

            _context.Applications.Update(newApplication);
            _context.SaveChanges();

            return Ok();
        }

        // POST: api/Applications
        [HttpPost]
        public IActionResult PostApplication(Application application)
        {
            var newApplication = new Application()
            {
                Code = application.Code,
                Description = application.Description,
                URL = application.URL,
                Users = application.Users
            };

            _context.Applications.Add(newApplication);
            _context.SaveChanges();

            return CreatedAtAction("GetApplication", new { id = newApplication.Id }, application);
        }

        // DELETE: api/Applications/5
        [HttpDelete("{id}")]
        public IActionResult DeleteApplication(long id)
        {
            var application = _context.Applications.FirstOrDefault(x => x.Id == id);

            if (application == null)
                return NotFound();

            _context.Applications.Remove(application);
            _context.SaveChanges();

            return NoContent();
        }
    }
}
