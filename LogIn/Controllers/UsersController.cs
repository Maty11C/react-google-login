﻿using System.Linq;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using LogIn.Models;
using LogIn.Services;

namespace LogIn.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class UsersController : ControllerBase
    {
        private readonly LogInDbContext _context;

        public UsersController(LogInDbContext context)
        {
            _context = context;
        }

        // GET: api/Users
        [HttpGet]
        public IActionResult GetUsers()
        {
            var users = _context.Users.Include(x => x.Applications).ToList();

            return Ok(users);
        }

        // GET: api/Users/5
        [HttpGet("{id}")]
        public IActionResult GetUser(long id)
        {
            var user = _context.Users.FirstOrDefault(x => x.Id == id);

            if (user == null)
                return NotFound();

            return Ok(user);
        }

        // PUT: api/Users/5
        [HttpPut("{id}")]
        public IActionResult PutUser(long id, User user)
        {
            if (id != user.Id)
                return BadRequest();

            var newUser = _context.Users.FirstOrDefault(x => x.Id == id);

            if (newUser == null)
                return NotFound();

            newUser.FirstName = user.FirstName;
            newUser.LastName = user.LastName;
            newUser.Email = user.Email;
            newUser.Password = user.Password;
            newUser.Applications = user.Applications;

            _context.Users.Update(newUser);
            _context.SaveChanges();

            return Ok();
        }

        // POST: api/Users
        [HttpPost]
        public IActionResult PostUser(User user)
        {
            var newUser = new User
            {
                FirstName = user.FirstName,
                LastName = user.LastName,
                Email = user.Email,
                Password = user.Password,
                Applications = user.Applications
            };

            _context.Users.Add(newUser);
            _context.SaveChanges();

            return CreatedAtAction("GetUser", new { id = newUser.Id }, user);
        }

        // DELETE: api/Users/5
        [HttpDelete("{id}")]
        public IActionResult DeleteUser(long id)
        {
            var user = _context.Users.FirstOrDefault(x => x.Id == id);

            if (user == null)
                return NotFound();

            _context.Users.Remove(user);
            _context.SaveChanges();

            return NoContent();
        }
        
    }
}
