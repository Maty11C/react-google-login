﻿using Microsoft.AspNetCore.Mvc;
using System.Linq;
using LogIn.DTOs;
using LogIn.Services;
using Microsoft.EntityFrameworkCore;

namespace LogIn.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class LoginController : ControllerBase
    {
        private readonly LogInDbContext _context;

        public LoginController(LogInDbContext context) => _context = context;

        [HttpPost]
        public IActionResult Login([FromBody] LoginDTO loginDTO)
        {
            var user = _context.Users.Include(x => x.Applications).FirstOrDefault(x => x.Email == loginDTO.Email);
            if (user == null)
                return NotFound($"No se encontró un usuario con email '{loginDTO.Email}'");

            var application = _context.Applications.FirstOrDefault(x => x.Code == loginDTO.ApplicationCode);
            if (application == null)
                return NotFound($"No se encontró una aplicación con código '{loginDTO.ApplicationCode}'");

            if (!user.AppAllowed(loginDTO.ApplicationCode))
                return Unauthorized($"El usuario {user.FullName()} no tiene permitido el acceso a la aplicación");

            // TODO: Crear JWT
            const string token =
                "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJmaXJzdE5hbWUiOiJNYXTDrWFzIiwibGFzdE5hbWUiOiJDYWJyZXJhIiwiZW1haWwiOiJtYXRpYXNyb2RyaWdvY2FicmVyYUBnbWFpbC5jb20ifQ.Xce_9ayuDKf3GwaYQWqJeNaVGEEHxZHngnk478FnoX8";

            var response = new
            {
                token = token,
                url = application.URL
            };
            return new ObjectResult(response);
        }
    }
}
