import "./App.css";
import Login from "./components/Login";
import { useLocation } from "react-router-dom";

function App() {
  const search = useLocation().search;
  const applicationCode = new URLSearchParams(search).get('applicationCode');

  const successHandler = (url) => {
    window.location.replace(url);
  };

  return (
    <div className="App">
        <h1>React Google Login</h1>
        <Login applicationCode={applicationCode} onSuccess={successHandler} />
    </div>
  );
}

export default App;
