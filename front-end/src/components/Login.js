import { useState } from "react";
import GoogleLogin, { GoogleLogout } from "react-google-login";
import classes from "./Login.module.css";
import axios from "axios";

const Login = (props) => {
  const [showloginButton, setShowloginButton] = useState(true);

  const loginSuccessHandler = (res) => {
    console.log("Login Success:", res.profileObj);

    const body = {
      Email: res.profileObj.email,
      ApplicationCode: props.applicationCode,
    };
    axios
      .post("https://localhost:44364/api/login", body)
      .then((res) => {
        const { token, url } = res.data;

        localStorage.setItem("token", token);
        setShowloginButton(false);

        props.onSuccess(url)
      })
      .catch((err) => {
        console.error(err);
      });
  };

  const loginFailureHandler = (res) => {
    console.log("Login Failed:", res);
  };

  const logoutSuccessHandler = () => {
    alert("You have been logged out successfully");
    console.clear();

    localStorage.removeItem("token");
    setShowloginButton(true);
  };

  return (
    <div className={classes.signin}>
      {showloginButton ? (
        <GoogleLogin
          clientId={process.env.REACT_APP_GOOGLE_CLIENT_ID}
          buttonText="Sign In"
          onSuccess={loginSuccessHandler}
          onFailure={loginFailureHandler}
          cookiePolicy={"single_host_origin"}
          isSignedIn={true}
        />
      ) : (
        <GoogleLogout
          clientId={process.env.REACT_APP_GOOGLE_CLIENT_ID}
          buttonText="Sign Out"
          onLogoutSuccess={logoutSuccessHandler}
        ></GoogleLogout>
      )}
    </div>
  );
};

export default Login;
